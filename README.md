# Big Project

## Popis

Big Project je projekt, který se zaměřuje na

- vytvoření správy gitlap projektu

## Požadavky

- Javafx verze 20
- Spring Boot verze 2.7.0
- Maven verze 4.2.3

## Instalace

1. Klonujte tento repozitář do vašeho lokálního stroje pomocí
   `git clone https://github.com/Madas42/big-project.git`.
2. Přejděte do složky projektu pomocí `cd big-project`.
3. Spusťte `mvn install` pro instalaci závislostí.
