package org.example;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Setter
    @Getter
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    @Column
    private String name;

    @Setter
    @Getter
    @Column
    private String email;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    public User(Long id, String name, String email){
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public User(){
    }

    @Override
    public String toString() {
        return "id: " + id +
                " , name: " + name +
                " , email: " + email;
    }
}