package org.example.controller;

import org.example.Comment;
import org.example.Post;
import org.example.service.CommentService;
import org.example.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;
    private final PostService postService;

    @Autowired
    public CommentController(CommentService commentService, PostService postService) {
        this.commentService = commentService;
        this.postService = postService;
    }

    @GetMapping
    public List<Comment> getComments() {
        return commentService.getComments();
    }

    @GetMapping("/{id}")
    public Comment getComment(@PathVariable Long id) {
        return commentService.getComment(id);
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable Long id) {
        commentService.deleteComment(id);
    }

    @PostMapping
    public Comment saveComment(@RequestBody Comment comment, @RequestParam Long postId) {
        Post post = postService.getPost(postId);
        if (post == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Post not found");
        }
        comment.setPost(post);
        return commentService.saveComment(comment);
    }
}