package org.example;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Post {
    @Setter
    @Getter
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String title;

    @Setter
    @Getter
    private String content;

//    @Setter
//    @Getter
//    @ManyToOne
//    @JoinColumn(name="user_id", nullable=false)
//    private Long userId;

    @Getter
    @Setter
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    @Setter
    @Getter
    @OneToMany(mappedBy = "post")
    @JsonManagedReference
    private List<Comment> comments;

    @Override
    public String toString() {
        return "id: " + id +
                " , title: " + title +
                " , content: " + content +
                " , userId: " + user;
    }
}