package org.example;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.repository.JpaRepository;
import javax.persistence.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Comment {
    @Setter
    @Getter
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Setter
    @Getter
    private String text;

    @Setter
    @Getter
    @ManyToOne
    @JoinColumn(name="post_id", nullable=false)
    @JsonBackReference
    private Post post;

    @Override
    public String toString() {
        return "id: " + id +
                " , text: " + text +
                " , postId: " + (post != null ? post.getId() : "null");
    }
}