package org.example;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class MainApp extends Application {

    private RestTemplate restTemplate = new RestTemplate();


    @Override
    public void start(Stage stage) {
        String usersUrl = "http://localhost:8080/users";
        User[] users = restTemplate.getForObject(usersUrl, User[].class);
        for (User user : users) {
            System.out.println(user.getName());
        }

        String postsUrl = "http://localhost:8080/posts";
        Post[] posts = restTemplate.getForObject(postsUrl, Post[].class);
        for (Post post : posts) {
            System.out.println(post.getTitle());
        }

        String commentsUrl = "http://localhost:8080/comments";
        Comment[] comments = restTemplate.getForObject(commentsUrl, Comment[].class);
        for (Comment comment : comments) {
            System.out.println(comment.getText());
        }

        TabPane tabPane = new TabPane();

        Tab userTab = new Tab("User");
        userTab.setContent(createUserUI());
        tabPane.getTabs().add(userTab);

        Tab postTab = new Tab("Post");
        postTab.setContent(createPostUI());
        tabPane.getTabs().add(postTab);

        Tab commentTab = new Tab("Comment");
        commentTab.setContent(createCommentUI());
        tabPane.getTabs().add(commentTab);

        Scene scene = new Scene(tabPane, 800, 600);
        stage.setScene(scene);
        stage.show();
    }

    private VBox createUserUI() {
        VBox vbox = new VBox();

        Label nameLabel = new Label("Name:");
        TextField nameInput = new TextField();
        vbox.getChildren().addAll(nameLabel, nameInput);

        Label emailLabel = new Label("Email:");
        TextField emailInput = new TextField();
        vbox.getChildren().addAll(emailLabel, emailInput);

        ListView<User> userList = new ListView<>();

        Button createUserButton = new Button("Create User");
        createUserButton.setOnAction(e -> {
            String name = nameInput.getText();
            String email = emailInput.getText();

            User newUser = new User();
            newUser.setName(name);
            newUser.setEmail(email);

            String usersUrl = "http://localhost:8080/users";
            User createdUser = restTemplate.postForObject(usersUrl, newUser, User.class);

            // Add the created user to the list view
            userList.getItems().add(createdUser);
        });
        vbox.getChildren().add(createUserButton);

        String usersUrl = "http://localhost:8080/users";
        User[] users = restTemplate.getForObject(usersUrl, User[].class);
        userList.getItems().addAll(users);
        vbox.getChildren().add(userList);

        return vbox;
    }

    private VBox createPostUI() {
        VBox vbox = new VBox();

        Label titleLabel = new Label("Title:");
        TextField titleInput = new TextField();
        vbox.getChildren().addAll(titleLabel, titleInput);

        Label contentLabel = new Label("Content:");
        TextField contentInput = new TextField();
        vbox.getChildren().addAll(contentLabel, contentInput);

        Label userIdLabel = new Label("User ID:");
        TextField userIdInput = new TextField();
        vbox.getChildren().addAll(userIdLabel, userIdInput);

        ListView<Post> postList = new ListView<>();

        Button createPostButton = new Button("Create Post");
        createPostButton.setOnAction(e -> {
            String title = titleInput.getText();
            String content = contentInput.getText();
            Long userId = Long.parseLong(userIdInput.getText());

            Post newPost = new Post();
            newPost.setTitle(title);
            newPost.setContent(content);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Post> entity = new HttpEntity<>(newPost, headers);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/posts")
                    .queryParam("userId", userId);

            Post createdPost = restTemplate.postForObject(builder.toUriString(), entity, Post.class);

            // Add the created post to the list view
            postList.getItems().add(createdPost);
        });
        vbox.getChildren().add(createPostButton);

        String postsUrl = "http://localhost:8080/posts";
        Post[] posts = restTemplate.getForObject(postsUrl, Post[].class);
        postList.getItems().addAll(posts);
        vbox.getChildren().add(postList);

        return vbox;
    }

    private VBox createCommentUI() {
        VBox vbox = new VBox();

        Label textLabel = new Label("Text:");
        TextField textInput = new TextField();
        vbox.getChildren().addAll(textLabel, textInput);

        Label postIdLabel = new Label("Post ID:");
        TextField postIdInput = new TextField();
        vbox.getChildren().addAll(postIdLabel, postIdInput);

        ListView<Comment> commentList = new ListView<>();

        Button createCommentButton = new Button("Create Comment");
        createCommentButton.setOnAction(e -> {
            String text = textInput.getText();
            Long postId = Long.parseLong(postIdInput.getText());

            Comment newComment = new Comment();
            newComment.setText(text);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Comment> entity = new HttpEntity<>(newComment, headers);

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/comments")
                    .queryParam("postId", postId);

            Comment createdComment = restTemplate.postForObject(builder.toUriString(), entity, Comment.class);

            // Add the created comment to the list view
            commentList.getItems().add(createdComment);
        });
        vbox.getChildren().add(createCommentButton);

        String commentsUrl = "http://localhost:8080/comments";
        Comment[] comments = restTemplate.getForObject(commentsUrl, Comment[].class);
        commentList.getItems().addAll(comments);
        vbox.getChildren().add(commentList);

        return vbox;
    }

    public static void main(String[] args) {
        launch(args);
    }
}