package org.example;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@Log4j2
@SpringBootApplication
public class App {

	private static ConfigurableApplicationContext context;

	public static void main(String[] args) {
		log.info("Launching Spring Boot application.");
		context = SpringApplication.run(App.class, args);
	}

	public static ConfigurableApplicationContext getApplicationContext() {
		return context;
	}
}