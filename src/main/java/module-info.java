module org.example {
	requires static lombok;
	requires org.apache.logging.log4j;
	requires java.persistence;
	requires spring.data.jpa;
	requires spring.beans;
	requires spring.context;
	requires spring.web;
	requires spring.boot;
	requires spring.boot.autoconfigure;
	requires javafx.controls;
	requires com.fasterxml.jackson.databind;
	requires com.fasterxml.jackson.annotation;

	exports org.example to spring.beans, javafx.graphics, com.fasterxml.jackson.databind;
}