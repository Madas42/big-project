FROM openjdk:17-jdk

LABEL maintainer="ces0023@vsb.cz"

RUN apt-get update && \
    apt-get install -y maven

ENV JAVA_HOME=/opt/openjdk-17

ENV PATH=$JAVA_HOME/bin:$PATH

WORKDIR /app

COPY . /app

RUN mvn install